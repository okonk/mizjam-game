import { Constants } from "../Constants";
import { DiceData } from "../objects/Dice/DiceData";
import { TextButton } from "../objects/TextButton";
import { Utils } from "../Utils";
import { NPC } from "../objects/NPC";

export class DialogScene extends Phaser.Scene {
  private text: string;
  private npc: NPC;
  private nextFunction: () => void;

  private textOffset: integer;
  private wrappedText: string[];

  private linesPerPage: integer = 5;

  private bitmapText: Phaser.GameObjects.BitmapText;
  private firstLoad: boolean = true;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.DIALOG,
    });
  }

  show() {
    this.scene.restart();
  }

  public init({ npc, text, next }) {
    this.npc = npc;
    this.text = text;
    this.nextFunction = next;
  }

  public create() {
    const width = 320;
    const height = 240/3;
    const offsetX = 0;
    const offsetY = 160;

    this.add.rectangle(offsetX, offsetY, width, height, 0xFFFFFF, 0.9)
      .setOrigin(0, 0)
      .setInteractive()
      .on("pointerup", () => this.next());

    this.wrappedText = this.wordwrap(this.text);
    this.textOffset = 0;

    this.bitmapText = this.add.bitmapText(135, 200, "bitfont", this.wrappedText.slice(0, this.linesPerPage)).setTint().setTint(0x111111).setOrigin(0.5, 0.5);

    this.add.image(280, 200, "colored_transparent", this.npc.icon).setTint(0x111111).setScale(2).setOrigin(0, 0.5);

    if (this.firstLoad) {
      var nextKey = this.input.keyboard.addKey("SPACE");
      nextKey.on("up", () => { this.next(); });

      this.firstLoad = false;
    }
  }

  private wordwrap(input: string): string[] {
    const wrapAt = 29;

    if (input.length < wrapAt) {
      return [input];
    }

    let remaining = input;
    let result = [];
    do {
      let lastSpace = remaining.lastIndexOf(" ", wrapAt);
      let substring = remaining.substr(0, lastSpace == -1 ? wrapAt : lastSpace);
      result.push(substring);
      remaining = remaining.substr(lastSpace + 1);
    } while (remaining.length >= wrapAt)

    result.push(remaining);

    return result;
  }

  private next() {
    this.textOffset++;

    if (this.textOffset * this.linesPerPage >= this.wrappedText.length) {
      this.scene.stop(Constants.DIALOG);
      this.nextFunction();
    }
    else {
      const start = this.textOffset * this.linesPerPage;
      this.bitmapText.setText(this.wrappedText.slice(start, start + this.linesPerPage));
    }
  }
}
