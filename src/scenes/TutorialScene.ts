import { Constants } from "../Constants";

export class TutorialScene extends Phaser.Scene {
  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.TUTORIAL,
    });
  }

  public create() {
    this.add.rectangle(0, 0, 320, 240, 0xFFFFFF, 0)
      .setOrigin(0, 0)
      .setInteractive()
      .on("pointerup", () => { this.scene.start(Constants.MENU); })

    this.input.keyboard.on("keyup", () => { this.scene.start(Constants.MENU); });

    this.add.bitmapText(0, 0, "bitfont", [
      "move: wasd or arrows",
      "interact: space",
      "inventory: i",
      "",
      "dice game:",
      "get points to beat the goal score",
      "and beat the opponents score",
      "if you get 3 \u0082 your turn is over",
      "and you lose your points that turn",
      "\u0080 = reroll this dice",
      "\u0081 = remove 1 skull",
      "\u0082 = skull",
      "\u0083 = 1 point\t \u0086 = 4 points",
      "\u0084 = 2 points \u0087 = 5 points",
      "\u0085 = 3 points \u0088 = 6 points",
    ]);
  }
}
