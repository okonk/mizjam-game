import { Constants } from "../Constants";
import { NPC } from "../objects/NPC";
import { Dice } from "../objects/Dice/Dice";
import { DiceData } from "../objects/Dice/DiceData";

export class DiceEndScene extends Phaser.Scene {
  private npc: NPC;
  private playerWon: boolean;

  private closeTime: integer;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.DICE_END,
    });
  }

  public init({ playerWon, npc }) {
    this.playerWon = playerWon;
    this.npc = npc;
  }

  public create() {
    this.cameras.main.setBackgroundColor(0x222222);

    const halfScreenX = 320/2;

    if (this.playerWon) {
      var diceData = this.takeDiceFromNPC();
      this.add.bitmapText(halfScreenX, 16, "bitfont", "you won").setOrigin(0.5, 0);

      var face = Math.max(...diceData.sides);
      this.add.image(halfScreenX + 3, 35, "colored_transparent", `colored_transparent-${face + 711}.png`)
        .setScale(2)
        .setOrigin(0.5, 0)
        .setTint(diceData.color);

      this.add.bitmapText(halfScreenX, 70, "bitfont", `from ${this.npc.name}`).setOrigin(0.5, 0);
    }
    else {
      var diceData = this.takeDiceFromPlayer();
      this.add.bitmapText(halfScreenX, 16, "bitfont", "you lost").setOrigin(0.5, 0);

      var face = Math.max(...diceData.sides);
      this.add.image(halfScreenX + 3, 35, "colored_transparent", `colored_transparent-${face + 711}.png`)
        .setScale(2)
        .setOrigin(0.5, 0)
        .setTint(diceData.color);

      this.add.bitmapText(halfScreenX, 70, "bitfont", `to ${this.npc.name}`).setOrigin(0.5, 0);
    }

    this.closeTime = Date.now() + 3500;
  }

  public update(time: number, delta: number) {
    super.update(time, delta);

    if (Date.now() >= this.closeTime) {
      this.scene.start(Constants.GAME);
    }
  }

  private takeDiceFromNPC() : DiceData {
    var dice = this.npc.dice;
    // Hack so player always wins magenta dice
    let toRemove = dice.findIndex(d => d.color == 0xff0097);
    if (toRemove == -1)
      toRemove = Phaser.Math.Between(0, dice.length - 1);
    const removed = dice[toRemove];
    dice.splice(toRemove, 1);
    this.npc.saveState(this.npc.map.getState());
    var playerData = this.registry.get("player");
    if (playerData.dice.length < 20)
      playerData.dice.push(removed);
    else
      playerData.inventory.push(removed);
    return removed;
  }

  private takeDiceFromPlayer() : DiceData {
    var playerData = this.registry.get("player");
    var dice = playerData.dice as DiceData[];
    const toRemove = Phaser.Math.Between(0, dice.length - 1);
    const removed = dice[toRemove];
    dice.splice(toRemove, 1);
    this.npc.dice.push(removed);
    this.npc.saveState(this.npc.map.getState());
    return removed;
  }
}
