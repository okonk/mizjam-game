import { Constants } from "../Constants";

export class CreditsScene extends Phaser.Scene {
  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.CREDITS,
    });
  }

  public create() {
    this.add.bitmapText(320/2, 240/2, "bitfont", [
      "Thank you for playing my game.",
      "I hope you enjoyed it.",
      "",
      "It took me 32 hours",
      "but I ran out of time to polish it.",
      "",
      "Thank you miziziziz for the jam",
      "and kenney.nl for the art",
      "I had fun making this game.",
      "",
      "Check out my main game",
      "lostaeon.com",
      "",
      ": Okonk"
    ], 16, 1).setOrigin(0.5, 0.5);

    this.add.rectangle(0, 0, 320, 240, 0xFFFFFF, 0)
      .setOrigin(0, 0)
      .setInteractive()
      .on("pointerup", () => { this.scene.start(Constants.MENU); })

    this.input.keyboard.on("keyup", () => { this.scene.start(Constants.MENU); });
  }
}
