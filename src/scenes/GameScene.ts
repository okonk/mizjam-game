import { Player } from "../objects/Player";
import { Map } from "../objects/Map";
import { Constants } from "../Constants";
import { Warp } from "../objects/Warp";
import { NPC } from "../objects/NPC";
import { KeyMap } from "../objects/KeyMap";

export class GameScene extends Phaser.Scene {
  private player: Player;
  private map: Map;
  private mapId: integer;
  private mapX: integer;
  private mapY: integer;
  private keys: any;
  private firstLoad: boolean = true;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.GAME,
    });
  }

  public init() {
    const playerData = this.registry.get("player");
    this.mapId = playerData.mapId;
    this.mapX = playerData.mapX;
    this.mapY = playerData.mapY;
  }

  public preload() {
    this.load.atlas("colored_transparent", "assets/images/colored_transparent.png", "assets/images/colored_transparent.json");
    this.load.tilemapTiledJSON(`map${this.mapId}`, `assets/maps/map${this.mapId}.json`);
  }

  public create() {
    const tileMap = this.make.tilemap({ key: `map${this.mapId}` });
    const tileset = tileMap.addTilesetImage("colored_transparent", "colored_transparent");

    const layer0 = tileMap.createStaticLayer("layer0", tileset).setDepth(-2);
    const layer1 = tileMap.createStaticLayer("layer1", tileset).setDepth(-1);
    const layer2 = tileMap.createStaticLayer("layer2", tileset).setDepth(200);

    if (this.firstLoad) {
      this.keys = this.input.keyboard.addKeys({
        inventory: 'I',
        down: 'DOWN',
        up: 'UP',
        left: 'LEFT',
        right: 'RIGHT',
        interact: 'SPACE',
        downAlt: 'S',
        upAlt: 'W',
        leftAlt: 'A',
        rightAlt: 'D'
      });

      this.keys.inventory.on("up", () => this.scene.start(Constants.INVENTORY));
      this.keys.interact.on("up", () => this.player.interact());

      this.events.on("shutdown", () => this.scene.stop(Constants.UI));

      this.firstLoad = false;
    }

    this.map = new Map(this, this.mapId, tileMap);

    this.player = new Player(this, this.mapX, this.mapY, this.map, this.keys);

    const camera = this.cameras.main;
    camera.setBounds(0, 0, tileMap.widthInPixels, tileMap.heightInPixels);
    camera.startFollow(this.player, true);
    camera.setFollowOffset(-Constants.HALF_TILE, -Constants.HALF_TILE);

    const bgColorHex = (tileMap.properties as any).find(p => p.name == "backgroundColor").value ?? "#00000000";
    const backgroundColor = Phaser.Display.Color.HexStringToColor('#' + bgColorHex.substring(3)).color32;
    camera.setBackgroundColor(backgroundColor);

    this.scene.launch(Constants.UI);
  }

  public update(time: number, delta: number) {
    this.map.update(time, delta);
  }
}
