import { Constants } from "../Constants";
import { Dice } from "../objects/Dice/Dice";
import { DiceFactory } from "../objects/Dice/DiceFactory";
import { TextButton } from "../objects/TextButton";
import { DiceFace } from "../objects/Dice/DiceFace";
import { NPC } from "../objects/NPC";
import { DiceBag } from "../objects/Dice/DiceBag";
import { DiceData } from "../objects/Dice/DiceData";

export class DiceScene extends Phaser.Scene {
  private npc: NPC;
  private npcDiceBag: DiceBag;
  private playerDiceBag: DiceBag;

  private visibleDice: Dice[];

  private rerollButtons: TextButton[] = [];

  private playerScore: integer = 0;
  private npcScore: integer = 0;
  private goalScore: integer = 0;

  private playerScoreText: Phaser.GameObjects.BitmapText;
  private npcScoreText: Phaser.GameObjects.BitmapText;
  private goalScoreText: Phaser.GameObjects.BitmapText;

  private playerImage: Phaser.GameObjects.Image;
  private npcImage: Phaser.GameObjects.Image;

  private currentScore: integer = 0;
  private currentSkulls: integer = 0;
  private currentScoreImage: Phaser.GameObjects.Image;
  private currentScoreText: Phaser.GameObjects.BitmapText;
  private currentSkullsText: Phaser.GameObjects.BitmapText;

  private rollButton: TextButton;
  private rollAgainButton: TextButton;
  private endTurnButton: TextButton;

  private failedText: Phaser.GameObjects.BitmapText;
  private failedNextTurnTimer: integer = 0;

  private playersTurn: boolean = true;
  private lastAITick: integer;
  private diceRolled: boolean = false;
  private turnText: Phaser.GameObjects.BitmapText;
  private turnTextTimer: integer = 0;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.DICE,
    });
  }

  public show() {
    this.scene.restart();
  }

  public init({ npc }) {
    this.npc = npc;
  }

  public preload() {

  }

  public create() {
    this.playersTurn = true;
    this.rerollButtons = [];
    this.playerScore = 0;
    this.npcScore = 0;
    this.currentScore = 0;
    this.currentSkulls = 0;

    const camera = this.cameras.main;
    camera.setBackgroundColor(0x777777);

    this.add.image(0, 0, "dicegui").setOrigin(0, 0);

    this.npcDiceBag = new DiceBag(this.npc.dice);
    this.playerDiceBag = new DiceBag(this.registry.get("player").dice);

    this.visibleDice = [new Dice(this, DiceFactory.get(0)), new Dice(this, DiceFactory.get(0)), new Dice(this, DiceFactory.get(0))];

    for (let i = 0; i < this.visibleDice.length; i++) {
      const dice = this.visibleDice[i];
      dice.setPosition((9 + i * 4) * 16, 48);
      dice.onFinishedRolling(result => this.updateRoundDice(result, i));
      this.add.existing(dice);
    }

    this.goalScore = this.npc.goalScore;

    this.playerScoreText = this.add.bitmapText(56, 2 * 16, "bitfont-numbers", " 0");
    this.npcScoreText = this.add.bitmapText(56, 4 * 16, "bitfont-numbers", " 0");
    this.goalScoreText = this.add.bitmapText(56, 6 * 16, "bitfont-numbers", " 0");

    this.playerImage = this.add.image(16, 32, "colored_transparent", "colored_transparent-504.png").setOrigin(0, 0);
    this.npcImage = this.add.image(16, 64, "colored_transparent", this.npc.icon).setOrigin(0, 0);

    this.add.bitmapText(5, 6 * 16, "bitfont", "goal");

    this.add.image(7 * 16, 10, "colored_transparent", "colored_transparent-713.png").setOrigin(0, 0).setTint(0xff0000);
    this.currentSkullsText = this.add.bitmapText(8 * 16, 10, "bitfont", "0").setTint(0xff0000);

    this.currentScoreImage = this.add.image(5 + 9 * 16, 10, "colored_transparent", "colored_transparent-504.png").setOrigin(0, 0).setTint(0x00ff00);
    this.currentScoreText = this.add.bitmapText(5 + 10 * 16, 10, "bitfont", "0").setTint(0x00ff00);

    this.createRerollButton(0);
    this.createRerollButton(1);
    this.createRerollButton(2);

    this.rollButton = new TextButton(this, 13 * 16, 70, "roll")
      .setOrigin(0.5, 0)
      .setInteractive()
      .on("pointerup", () => this.rollClicked())
      .setVisible(false);

    this.rollAgainButton = new TextButton(this, 13 * 16, 86, "roll again")
      .setOrigin(0.5, 0)
      .setInteractive()
      .on("pointerup", () => this.rollClicked())
      .setVisible(false);

    this.endTurnButton = new TextButton(this, 13 * 16, 102, "end turn")
      .setOrigin(0.5, 0)
      .setInteractive()
      .on("pointerup", () => this.endTurnClicked())
      .setVisible(false);

    this.failedText = this.add.bitmapText(13 * 16, 86, "bitfont", "round lost")
      .setTint(0xff0000)
      .setOrigin(0.5, 0)
      .setVisible(false);

    this.turnText = this.add.bitmapText(13 * 16, 86, "bitfont", "your turn")
      .setOrigin(0.5, 0)
      .setVisible(false);

    this.refreshScores();
    this.setPlayersTurn();
  }

  private createRerollButton(i: integer) {
    const x = (9 + i * 4) * 16;

    const button = new TextButton(this, x, 70, "reroll")
      .setOrigin(0.5, 0)
      .on("pointerup", () => this.rerollClicked(i))
      .setVisible(false);

    this.rerollButtons.push(button);
  }

  public update(time: number, delta: number) {
    for (let dice of this.visibleDice) {
      dice.update(time, delta);
    }

    if (this.failedNextTurnTimer > 0 && Date.now() >= this.failedNextTurnTimer) {
      this.moveToNextTurn();
    }

    if (this.turnTextTimer > 0 && Date.now() >= this.turnTextTimer) {
      this.startPlayersTurn();
    }

    if (!this.playersTurn) {
      this.doAILogic();
    }
  }

  private setNPCsTurn() {
    this.currentScoreImage.setTexture("colored_transparent", this.npc.icon);
    this.turnText.text = `${this.npc.name}s turn`;
    this.turnText.setVisible(true);
    this.lastAITick = Date.now() + 1000;
  }

  private setPlayersTurn() {
    this.currentScoreImage.setTexture("colored_transparent", "colored_transparent-504.png");
    this.turnText.text = "your turn";
    this.turnTextTimer = Date.now() + 1500;
    this.turnText.setVisible(true);
  }

  private startPlayersTurn() {
    this.turnTextTimer = 0;
    this.turnText.setVisible(false);
    this.rollButton.setVisible(true);
  }

  private rerollClicked(i: integer) {
    this.rerollButtons[i].setVisible(false);
    this.hideButtons(false);
    this.visibleDice[i].roll();
  }

  private rollClicked() {
    this.drawDice();
    this.hideButtons();

    for (let dice of this.visibleDice) {
      dice.roll();
    }
  }

  private drawDice() {
    let dice: DiceData[];
    if (this.playersTurn)
      dice = this.playerDiceBag.draw();
    else
      dice = this.npcDiceBag.draw();

    for (let i = 0; i < this.visibleDice.length; i++) {
      const d = dice[i];
      this.visibleDice[i].setDiceData(d);
    }
  }

  private endTurnClicked() {
    if (this.playersTurn) {
      this.playerScore += this.currentScore;
    }
    else {
      this.npcScore += this.currentScore;
    }

    this.moveToNextTurn();
  }

  private moveToNextTurn() {
    this.playersTurn = !this.playersTurn;

    if (this.playersTurn) {
      if (this.playerScore == this.npcScore && this.playerScore >= this.goalScore) {
        this.setPlayersTurn(); // tie breaker
      }
      else if (this.playerScore >= this.goalScore || this.npcScore >= this.goalScore) {
        this.scene.start(Constants.DICE_END, { playerWon: this.playerScore > this.npcScore, npc: this.npc });
      }
      else {
        this.setPlayersTurn();
      }
    }
    else {
      this.setNPCsTurn();
    }

    this.resetState();
  }

  private onFailed() {
    this.hideButtons();
    this.turnText.setVisible(false);
    this.failedText.setVisible(true);
    this.failedNextTurnTimer = Date.now() + 1500;
  }

  private resetState() {
    this.currentScore = 0;
    this.currentSkulls = 0;
    this.diceRolled = false;
    this.failedNextTurnTimer = 0;
    this.failedText.setVisible(false);
    this.hideButtons();

    this.refreshGameArea();
    this.refreshScores();
  }

  private refreshScores() {
    this.playerScoreText.text = (this.playerScore < 10 ? " " : "") + this.playerScore;
    this.npcScoreText.text = (this.npcScore < 10 ? " " : "") + this.npcScore;
    this.goalScoreText.text = (this.goalScore < 10 ? " " : "") + this.goalScore;
  }

  private refreshGameArea() {
    this.currentScoreText.text = this.currentScore.toString();
    this.currentSkullsText.text = this.currentSkulls.toString();
  }

  private diceFinishedRolling() {
    this.currentSkulls = Math.max(0, this.currentSkulls);
    this.diceRolled = true;

    this.refreshGameArea();

    if (this.currentSkulls >= 3 && this.noBlanks()) {
      this.onFailed();
    }
    else if (this.playersTurn) {
      if (this.currentSkulls < 3)
        this.rollAgainButton.setVisible(true);
      this.endTurnButton.setVisible(true);
    }
  }

  private updateRoundDice(roll: DiceFace, i: integer) {
    if (roll == DiceFace.Skull) {
      this.currentSkulls++;
    }
    else if (roll == DiceFace.Cross) {
      this.currentSkulls--; // TODO: Need a mechanic for cross, removing skulls is too OP
    }
    else if (roll == DiceFace.Blank) {
      if (this.playersTurn)
        this.rerollButtons[i].setVisible(true);
    }
    else {
      this.currentScore += roll - 2;
    }

    if (!this.anyDiceRolling())
      this.diceFinishedRolling();
  }

  private anyDiceRolling() : boolean {
    let stillRolling = false;
    for (let dice of this.visibleDice) {
      stillRolling = stillRolling || dice.isRolling();
    }

    return stillRolling;
  }

  private noBlanks() : boolean {
    for (let dice of this.visibleDice) {
      if (dice.getRoll() == DiceFace.Blank)
        return false;
    }

    return true;
  }

  private hideButtons(hideRerolls: boolean = true) {
    this.rollAgainButton.setVisible(false);
    this.rollButton.setVisible(false);
    this.endTurnButton.setVisible(false);

    if (hideRerolls) {
      for (let reroll of this.rerollButtons)
        reroll.setVisible(false);
    }
  }

  private doAILogic() {
    if (Date.now() - this.lastAITick < 2000) return;

    const rolling = this.anyDiceRolling();
    if (!rolling && this.failedNextTurnTimer <= 0) {
      if (this.diceRolled) {
        let reroll = false;
        for (let i = 0; i < this.visibleDice.length; i++) {
          const dice = this.visibleDice[i];

          if (dice.getRoll() == DiceFace.Blank) {
            this.rerollClicked(i);
            reroll = true;
          }
        }

        if (!reroll) {
          const currentTotal = this.currentScore + this.npcScore;
          if (this.currentSkulls == 2 || (currentTotal >= this.goalScore && currentTotal > this.playerScore)) {
            this.endTurnClicked();
          }
          else {
            this.rollClicked();
          }
        }
      }
      else {
        this.rollClicked();
      }
    }

    this.lastAITick = Date.now();
  }
}
