import { Constants } from "../Constants";
import { DiceData } from "../objects/Dice/DiceData";
import { TextButton } from "../objects/TextButton";
import { Utils } from "../Utils";

export class DiceInfoScene extends Phaser.Scene {
  private diceData: DiceData;
  private isDiceBag: boolean;
  private index: integer;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.DICE_INFO,
    });
  }

  show() {
    this.scene.restart();
  }

  public init({ diceData, isDiceBag, index }) {
    this.diceData = diceData;
    this.isDiceBag = isDiceBag;
    this.index = index;
  }

  public create() {
    const width = 214;
    const height = 120;
    const offsetX = 320 / 2 - width / 2;
    const offsetY = 240 / 2 - height / 2;

    this.add.rectangle(0, 0, 320, 240, 0x111111, 0.9).setOrigin(0, 0);
    this.add.rectangle(offsetX, offsetY, width, height, 0x555555, 0.9).setOrigin(0, 0);

    for (let i = 0; i < this.diceData.sides.length; i++) {
      var face = this.diceData.sides[i];
      var diceSide = this.add.image(offsetX + 10 + i * 32, offsetY + 10, "colored_transparent", `colored_transparent-${face + 711}.png`)
        .setScale(2)
        .setOrigin(0, 0)
        .setTint(this.diceData.color);
    }

    const moveButton = new TextButton(this, offsetX + width / 2, offsetY + 50, `move to ${this.isDiceBag ? "storage" : "equipped"}`)
      .setOrigin(0.5, 0)
      .on("pointerup", () => this.moveDice());

    const disableMove = (this.isDiceBag && this.registry.get("player").dice.length <= 6) ||
      (!this.isDiceBag && this.registry.get("player").dice.length >= 20);

    if (disableMove) {
      moveButton.disableInteractive();
      moveButton.setColor(0x444444);
    }

    const closeButton = new TextButton(this, offsetX + width / 2, offsetY + 70, "close")
      .setOrigin(0.5, 0)
      .on("pointerup", () => this.closeButtonClicked());
  }

  private closeButtonClicked() {
    this.scene.stop(Constants.DICE_INFO);
    this.scene.resume(Constants.INVENTORY);
  }

  private moveDice() {
    if (this.isDiceBag) {
      var diceBag: DiceData[] = this.registry.get("player").dice;
      var removed = Utils.removeDice(diceBag, this.index);

      var inventory: DiceData[] = this.registry.get("player").inventory;
      inventory.push(removed);
    }
    else {
      var inventory: DiceData[] = this.registry.get("player").inventory;
      var removed = Utils.removeDice(inventory, this.index);

      var diceBag: DiceData[] = this.registry.get("player").dice;
      diceBag.push(removed);
    }

    this.scene.stop(Constants.DICE_INFO);
    this.scene.start(Constants.INVENTORY);
  }
}
