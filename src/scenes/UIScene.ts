import { Constants } from "../Constants";

export class UIScene extends Phaser.Scene {
  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.UI,
    });
  }

  public create() {
    const background = this.add.rectangle(310, 8, 20, 20, 0x111111, 0.7)
      .setOrigin(0.5, 0.5);
    const inventoryIcon = this.add.image(310, 8, "colored_transparent", "colored_transparent-476.png")
      .setInteractive()
      .setTint(0xCCCCCC)
      .on("pointerup", () => { this.scene.start(Constants.INVENTORY); })
      .on("pointerover", () => { inventoryIcon.setTint(0x999999) })
      .on("pointerout", () => { inventoryIcon.setTint(0xCCCCCC) });
  }
}
