import { Constants } from "../Constants";
import { ScrollablePanel } from 'phaser3-rex-plugins/templates/ui/ui-components.js';
import { Dice } from "../objects/Dice/Dice";
import { TextButton } from "../objects/TextButton";
import { DiceData } from "../objects/Dice/DiceData";

export class InventoryScene  extends Phaser.Scene {
  private diceBagPanel: ScrollablePanel;
  private storagePanel: ScrollablePanel;
  private rexUI: any;

  private firstLoad: boolean = true;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.INVENTORY,
    });
  }

  public create() {
    const camera = this.cameras.main;
    camera.setBackgroundColor(0x777777);

    this.createDiceBagUI();
    this.createStorageUI();

    const closeButton = new TextButton(this, 320/2, 210, "close")
      .setOrigin(0.5, 0)
      .on("pointerup", () => { this.closeButtonClicked(); });

    if (this.firstLoad) {
      var inventoryKey = this.input.keyboard.addKey("I");
      inventoryKey.on("up", () => { this.closeButtonClicked(); });

      this.firstLoad = false;
    }
  }

  private createDiceBagUI() {
    this.add.bitmapText(0, 0, "bitfont", "equipped");
    this.diceBagPanel = new ScrollablePanel(this, {
      x: 0,
      y: 20,
      width: 140,
      height: 180,

      scrollMode: 0,

      //background: this.rexUI.add.roundRectangle(0, 0, 2, 2, 10, 0xFFFFFF),

      panel: {
          child: this.rexUI.add.fixWidthSizer({
              space: {
                  left: 3,
                  right: 3,
                  top: 3,
                  bottom: 3,
                  item: 8,
                  line: 8,
              }
          }),

          mask: {
              padding: 0
          },
      },

      slider: {
          track: this.rexUI.add.roundRectangle(0, 0, 15, 10, 2, 0x555555),
          thumb: this.rexUI.add.roundRectangle(0, 0, 15, 15, 5, 0x333333),
      },

      space: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,

          panel: 0,
      }
    }).setOrigin(0, 0).layout();

    var sizer = this.diceBagPanel.getElement("panel");
    var diceBag: DiceData[] = this.registry.get("player").dice;
    for (let i = 0; i < diceBag.length; i++) {
      const diceData = diceBag[i];
      var dice = new Dice(this, diceData)
        .setInteractive()
        .on("pointerup", () => this.onDiceClicked(diceData, true, i));
      this.add.existing(dice);
      sizer.add(dice);
    }

    this.diceBagPanel.layout();
  }

  private createStorageUI() {
    this.add.bitmapText(160, 0, "bitfont", "storage");
    this.storagePanel = new ScrollablePanel(this, {
      x: 160,
      y: 20,
      width: 140,
      height: 180,

      scrollMode: 0,

      //background: this.rexUI.add.roundRectangle(0, 0, 2, 2, 10, 0xFFFFFF),

      panel: {
          child: this.rexUI.add.fixWidthSizer({
              space: {
                  left: 3,
                  right: 3,
                  top: 3,
                  bottom: 3,
                  item: 8,
                  line: 8,
              }
          }),

          mask: {
              padding: 0
          },
      },

      slider: {
          track: this.rexUI.add.roundRectangle(0, 0, 15, 10, 2, 0x555555),
          thumb: this.rexUI.add.roundRectangle(0, 0, 15, 15, 5, 0x333333),
      },

      space: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,

          panel: 0,
      }
    }).setOrigin(0, 0).layout();

    var sizer = this.storagePanel.getElement("panel");
    var inventory: DiceData[] = this.registry.get("player").inventory;
    for (let i = 0; i < inventory.length; i++) {
      const diceData = inventory[i];
      var dice = new Dice(this, diceData)
        .setInteractive()
        .on("pointerup", () => this.onDiceClicked(diceData, false, i));
      this.add.existing(dice);
      sizer.add(dice);
    }

    this.storagePanel.layout();
  }

  private onDiceClicked(diceData: DiceData, isDiceBag: boolean, index: integer) {
    this.scene.pause(Constants.INVENTORY);
    this.scene.launch(Constants.DICE_INFO, { diceData, isDiceBag, index });
  }

  private closeButtonClicked() {
    this.scene.start(Constants.GAME);
  }
}
