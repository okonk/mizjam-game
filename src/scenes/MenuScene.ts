import { Constants } from "../Constants";
import { TextButton } from "../objects/TextButton";
import { DiceFactory } from "../objects/Dice/DiceFactory";

export class MenuScene extends Phaser.Scene {

  private firstLoad: boolean = true;

  constructor() {
    super({
      active: false,
      visible: false,
      key: Constants.MENU,
    });
  }

  public preload() {
    this.load.atlas("colored_transparent", "assets/images/colored_transparent.png", "assets/images/colored_transparent.json");
    this.load.bitmapFont("bitfont", "assets/images/colored_transparent.png", "assets/fonts/bitfont.xml");
    this.load.bitmapFont("bitfont-numbers", "assets/images/colored_transparent.png", "assets/fonts/bitfont-numbers.xml");
    this.load.image("dicegui", "assets/images/dicegui.png");
  }

  public create() {
    this.cameras.main.setBackgroundColor(0x777777);

    if (this.firstLoad) {
      this.initialiseRegistry();
      this.firstLoad = false;
    }

    new TextButton(this, this.cameras.main.width/2, this.cameras.main.height/2 - 20, "Play Game")
      .setOrigin(0.5, 0.5)
      .setInteractive()
      .on("pointerup", () => this.scene.start(Constants.GAME));

    new TextButton(this, this.cameras.main.width/2, this.cameras.main.height/2, "Tutorial")
      .setOrigin(0.5, 0.5)
      .setInteractive()
      .on("pointerup", () => this.scene.start(Constants.TUTORIAL));

    new TextButton(this, this.cameras.main.width/2, this.cameras.main.height/2 + 20, "Credits")
      .setOrigin(0.5, 0.5)
      .setInteractive()
      .on("pointerup", () => this.scene.start(Constants.CREDITS));
  }

  private initialiseRegistry() {
    this.registry.set("player", {
      mapId: 1,
      mapX: 9,
      mapY: 18,
      inventory: [],
      dice: [DiceFactory.get(1), DiceFactory.get(1), DiceFactory.get(2), DiceFactory.get(2), DiceFactory.get(2), DiceFactory.get(3)],
      hasCompletedGame: false,
    });

    this.registry.set("maps", {});
  }
}
