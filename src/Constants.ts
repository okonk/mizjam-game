export class Constants {
  public static readonly TILE_SIZE = 16;
  public static readonly HALF_TILE = 16 / 2;

  public static readonly GAME = 'Game';
  public static readonly DICE = 'Dice';
  public static readonly MENU = 'Menu';
  public static readonly DICE_END = 'DiceEnd';
  public static readonly INVENTORY = 'Inventory';
  public static readonly DICE_INFO = 'DiceInfo';
  public static readonly DIALOG = 'Dialog';
  public static readonly CREDITS = 'Credits';
  public static readonly TUTORIAL = 'Tutorial';
  public static readonly UI = 'UI';
}
