import * as Phaser from 'phaser';
import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin.js';

import { GameScene } from './scenes/GameScene';
import { DiceScene } from './scenes/DiceScene';
import { MenuScene } from './scenes/MenuScene';
import { DiceEndScene } from './scenes/DiceEndScene';
import { InventoryScene } from './scenes/InventoryScene';
import { DiceInfoScene } from './scenes/DiceInfoScene';
import { DialogScene } from './scenes/DialogScene';
import { UIScene } from './scenes/UIScene';
import { TutorialScene } from './scenes/TutorialScene';
import { CreditsScene } from './scenes/CreditsScene';

const gameConfig: Phaser.Types.Core.GameConfig = {
  title: 'Lost Dice',

  type: Phaser.WEBGL,

  scale: {
    mode: Phaser.Scale.FIT,
    width: 320,
    height: 240,
  },

  render: {
    pixelArt: true,
  },

  parent: 'game-container',
  backgroundColor: '#444444',

  scene: [MenuScene, GameScene, DiceScene, DiceEndScene, InventoryScene, DiceInfoScene, DialogScene, UIScene, CreditsScene, TutorialScene],

  plugins: {
    scene: [{
      key: 'rexUI',
      plugin: UIPlugin,
      mapping: 'rexUI'
    }]
  }
};

export const game = new Phaser.Game(gameConfig);
