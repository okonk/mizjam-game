import { DiceData } from "./DiceData";

export class DiceBag {
  private dice: DiceData[];
  private drawnDice: DiceData[];

  constructor(dice: DiceData[]) {
    this.dice = [...dice];
    this.dice.length = dice.length;
    this.drawnDice = [];
  }

  public draw(): DiceData[] {
    if (this.dice.length < 3) {
      this.dice = this.dice.concat(this.drawnDice);
      this.drawnDice = [];
    }

    var result = [];
    for (let i = 0; i < 3; i++) {
      const toRemove = Phaser.Math.Between(0, this.dice.length - 1);
      result.push(this.dice[toRemove]);
      this.drawnDice.push(this.dice[toRemove]);
      this.dice.splice(toRemove, 1);
    }

    return result;
  }
}
