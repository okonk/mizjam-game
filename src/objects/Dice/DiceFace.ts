export enum DiceFace {
  Blank = 0,
  Cross,
  Skull,
  One,
  Two,
  Three,
  Four,
  Five,
  Six
}
