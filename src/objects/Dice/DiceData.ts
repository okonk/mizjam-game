import { DiceFace } from "./DiceFace";

export class DiceData {
  public sides: DiceFace[];
  public color: integer;

  constructor(color: integer, sides: DiceFace[]) {
    this.color = color;
    this.sides = sides;
  }
}
