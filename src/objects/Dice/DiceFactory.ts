import { DiceData } from "./DiceData";
import { DiceFace } from "./DiceFace";

export class DiceFactory {
  public static get(id: integer): DiceData {
    switch (id) {
      case 0:
        return new DiceData(0xffffff, [DiceFace.Blank, DiceFace.Blank, DiceFace.Blank, DiceFace.Blank, DiceFace.Blank, DiceFace.Blank]);
      case 1:
        return new DiceData(0x99b433, [DiceFace.Blank, DiceFace.Blank, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.One]);
      case 2:
        return new DiceData(0x00a300, [DiceFace.Blank, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.One]);
      case 3:
        return new DiceData(0x1e7145, [DiceFace.Blank, DiceFace.Cross, DiceFace.Skull, DiceFace.One, DiceFace.One, DiceFace.One]);
      case 4:
        return new DiceData(0x9f00a7, [DiceFace.Blank, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.Two]);
      case 5:
        return new DiceData(0x7e3878, [DiceFace.Cross, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.Two]);
      case 6:
        return new DiceData(0x603cba, [DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.Two, DiceFace.Two]);
      case 7:
        return new DiceData(0x1d1d1d, [DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.One, DiceFace.Two, DiceFace.Three]);
      case 8:
        return new DiceData(0x00aba9, [DiceFace.Blank, DiceFace.Skull, DiceFace.Skull, DiceFace.Two, DiceFace.Three, DiceFace.Three]);
      case 9:
        return new DiceData(0x2d89ef, [DiceFace.Cross, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.Three, DiceFace.Four]);
      case 10:
        return new DiceData(0x2b5797, [DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.Three, DiceFace.Four, DiceFace.Four]);
      case 11:
        return new DiceData(0xffc40d, [DiceFace.Cross, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.Four, DiceFace.Five]);
      case 12:
        return new DiceData(0xe3a21a, [DiceFace.Blank, DiceFace.Skull, DiceFace.Skull, DiceFace.Three, DiceFace.Four, DiceFace.Five]);
      case 13:
        return new DiceData(0xda532c, [DiceFace.Blank, DiceFace.Skull, DiceFace.Skull, DiceFace.Four, DiceFace.Five, DiceFace.Five]);
      case 14:
        return new DiceData(0xee1111, [DiceFace.Blank, DiceFace.Cross, DiceFace.Skull, DiceFace.Skull, DiceFace.Five, DiceFace.Six]);
      case 15:
        return new DiceData(0xb91d47, [DiceFace.Cross, DiceFace.Cross, DiceFace.Skull, DiceFace.Four, DiceFace.Five, DiceFace.Six]);
      case 16:
        return new DiceData(0xff0097, [DiceFace.Six, DiceFace.Six, DiceFace.Six, DiceFace.Six, DiceFace.Six, DiceFace.Six]);
    }

    return undefined;
  }
}
