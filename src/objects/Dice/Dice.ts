import { DiceFace } from "./DiceFace";
import { DiceData } from "./DiceData";

export class Dice extends Phaser.GameObjects.Sprite {
  private dice: DiceData;
  private currentRoll: integer;

  private rolling: boolean;
  private rollStop: integer;
  private nextRoll: integer;

  private finishedRolling: (roll: DiceFace) => void;

  constructor(scene: Phaser.Scene, dice: DiceData) {
    super(scene, 0, 0, "colored_transparent", "colored_transparent-711.png");

    this.dice = dice;
    this.setRollValue();
    this.rolling = false;

    this.setScale(2);
    this.setTint(this.dice.color);
    this.setTexture("colored_transparent", `colored_transparent-${this.sideToTextureId()}.png`);
  }

  public roll() {
    this.rolling = true;
    this.rollStop = Date.now() + Phaser.Math.Between(1500, 2500);
    this.nextRoll = Date.now();
  }

  private setRollValue() {
    this.currentRoll = Phaser.Math.Between(0, this.dice.sides.length - 1);
  }

  private updateRoll() {
    if (!this.rolling || Date.now() < this.nextRoll) return;

    this.nextRoll = Date.now() + Phaser.Math.Between(50, 100);

    const oldRoll = this.currentRoll;
    do {
      this.setRollValue();
    } while (oldRoll == this.currentRoll)

    if (Date.now() >= this.rollStop) {
      this.rolling = false;
      // reroll to be more fair
      this.setRollValue();

      this.finishedRolling(this.getRoll());
    }

    this.setTexture("colored_transparent", `colored_transparent-${this.sideToTextureId()}.png`);
  }

  private sideToTextureId() {
    return this.getRoll() + 711;
  }

  public update(time: integer, delta: integer) {
    this.updateRoll();
  }

  public getRoll() : DiceFace {
    return this.dice.sides[this.currentRoll];
  }

  public isRolling() : boolean {
    return this.rolling;
  }

  public onFinishedRolling(callback: (result: DiceFace) => void) {
    this.finishedRolling = callback;
  }

  public setDiceData(dice: DiceData) {
    this.dice = dice;
    this.setRollValue();

    this.setTint(this.dice.color);
    this.setTexture("colored_transparent", `colored_transparent-${this.sideToTextureId()}.png`);
  }
}
