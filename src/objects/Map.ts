import { Entity } from "./Entity";
import { Warp } from "./Warp";
import { Constants } from "../Constants";
import { NPC } from "./NPC";
import { EntityType } from "./EntityType";
import { NPCFactory } from "./NPCFactory";

export class Map {
  private id: integer;
  private tileMap: Phaser.Tilemaps.Tilemap;
  private entities: Entity[] = [];
  private warps: { [id: number]: Warp; } = { };
  private scene: Phaser.Scene;

  constructor(scene: Phaser.Scene, id: integer, map: Phaser.Tilemaps.Tilemap) {
    this.id = id;
    this.tileMap = map;
    this.scene = scene;

    this.loadWarps();
    this.loadNPCs();

    this.scene.events.on("shutdown", () => this.onSceneShutdown());
  }

  private loadWarps() {
    var objectLayer = this.tileMap.getObjectLayer("objects");
    for (let o of objectLayer.objects) {
      if (o.type != 'Warp')  continue;

      const id = o.properties.find(p => p.name == "id")?.value ?? 1;
      const x = o.properties.find(p => p.name == "x")?.value ?? 10;
      const y = o.properties.find(p => p.name == "y")?.value ?? 10;

      const ox = o.x / Constants.TILE_SIZE;
      const oy = o.y / Constants.TILE_SIZE;
      const w = o.width / Constants.TILE_SIZE;
      const h = o.height / Constants.TILE_SIZE;

      for (let wx = ox; wx < ox + w; wx++) {
        for (let wy = oy; wy < oy + w; wy++) {
          const destx = x + wx - ox;
          const desty = y + wy - oy;

          this.warps[wy * this.tileMap.width + wx] = new Warp(id, destx, desty);
        }
      }
    }
  }

  private loadNPCs() {
    var objectLayer = this.tileMap.getObjectLayer("objects");
    for (let o of objectLayer.objects) {
      if (o.type != 'NPC')  continue;

      const id = o.properties?.find(p => p.name == "id")?.value ?? 0;
      const npcData = NPCFactory.get(id);

      const ox = o.x / Constants.TILE_SIZE;
      const oy = o.y / Constants.TILE_SIZE;

      this.add(new NPC(this.scene, id, npcData, ox, oy, this));
    }
  }

  public getWarp(x: integer, y: integer) : Warp {
    return this.warps[this.tileMap.width * y + x];
  }

  public add(entity: Entity) {
    this.entities.push(entity);
  }

  public canMove(x: number, y: number, canMoveOnWarp: boolean = false) : boolean {
    if (x < 0 || x >= this.tileMap.width || y < 0 || y >= this.tileMap.height)
      return false;

    if (this.tileMap.getLayer("blocked").data[y][x].index != -1)
      return false;

    if (!canMoveOnWarp && this.getWarp(x, y))
      return false;

    if (this.getEntity(x, y))
      return false;

    return true;
  }

  public update(time: number, delta: number) {
    for (let entity of this.entities)
      entity.update(time, delta);
  }

  public getEntity(x: integer, y: integer) : Entity {
    return this.entities.find(e => e.tileX == x && e.tileY == y);
  }

  private onSceneShutdown() {
    let mapsState = this.scene.registry.get("maps");
    let mapState = mapsState[this.id] ?? {};

    for (let entity of this.entities) {
      if (entity.entityType == EntityType.Player) continue;

      entity.saveState(mapState);
    }

    mapsState[this.id] = mapState;
  }

  public getState() {
    let mapsState = this.scene.registry.get("maps");
    let mapState = mapsState[this.id] ?? {};

    return mapState;
  }
}
