import { Constants } from "../Constants";
import { Map } from "../objects/Map";
import { EntityType } from "./EntityType";

const Vector2 = Phaser.Math.Vector2;
type Vector2 = Phaser.Math.Vector2;

export class Entity extends Phaser.GameObjects.Sprite {
  public tileX: integer;
  public tileY: integer;
  public entityType: EntityType;

  protected facing: Phaser.Math.Vector2 = Vector2.DOWN;

  protected moveDirection: Phaser.Math.Vector2;
  private posX: number;
  private posY: number;
  private goalX: integer;
  private goalY: integer;

  protected speed: number;

  public map: Map;

  constructor(scene: Phaser.Scene, graphicId: integer, tileX: integer, tileY: integer, map: Map) {
    super(scene, tileX * Constants.TILE_SIZE + Constants.HALF_TILE, tileY * Constants.TILE_SIZE + Constants.HALF_TILE, "colored_transparent", `colored_transparent-${graphicId}.png`);

    this.map = map;
    this.tileX = tileX;
    this.tileY = tileY;
    this.moveDirection = Vector2.ZERO;
    this.posX = this.x;
    this.posY = this.y;

    this.speed = 200;

    scene.add.existing(this);

    this.setDepth(100);

    this.map.add(this);
  }

  public update(time: number, delta: number) {
    this.updatePosition(delta);
  }

  public isMoving() : boolean {
    return this.moveDirection != Vector2.ZERO;
  }

  public move(direction: Vector2) : boolean {
    this.facing = direction;

    if (!this.canMove(direction)) {
      return false;
    }

    this.moveDirection = direction;

    this.tileX += this.moveDirection.x;
    this.tileY += this.moveDirection.y;

    this.goalX = this.tileX * Constants.TILE_SIZE + Constants.HALF_TILE;
    this.goalY = this.tileY * Constants.TILE_SIZE + Constants.HALF_TILE;

    return true;
  }

  private updatePosition(delta: number) {
    if (this.moveDirection == Vector2.ZERO) return;

    const move = (Constants.TILE_SIZE * (delta / this.speed));
    this.posX += this.moveDirection.x * move;
    this.posY += this.moveDirection.y * move;

    const reachedX = this.moveDirection.x == 0 || (this.moveDirection.x < 0 && this.posX <= this.goalX) || (this.moveDirection.x > 0 && this.posX >= this.goalX);
    const reachedY = this.moveDirection.y == 0 || (this.moveDirection.y < 0 && this.posY <= this.goalY) || (this.moveDirection.y > 0 && this.posY >= this.goalY);

    if (reachedX && reachedY) {
      this.setPosition(this.goalX, this.goalY);
      this.moveDirection = Vector2.ZERO;
      this.posX = this.goalX;
      this.posY = this.goalY;
      this.finishedMove();
      return;
    }

    this.setPosition(Math.floor(this.posX), Math.floor(this.posY));
  }

  private canMove(direction: Vector2) : boolean {
    const newX = this.tileX + direction.x;
    const newY = this.tileY + direction.y;

    return this.map.canMove(newX, newY, this.entityType == EntityType.Player);
  }

  protected finishedMove() {

  }

  public saveState(storage: any) {

  }
}
