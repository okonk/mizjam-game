export class TextButton extends Phaser.GameObjects.Shape {
  private text: Phaser.GameObjects.BitmapText;
  private color: integer = 0xffffff;
  private hoverColor: integer = 0x222222;

  constructor(scene: Phaser.Scene, x: integer, y: integer, label: string) {
    super(scene, "TextButton");

    this.text = scene.add.bitmapText(x, y, "bitfont", label);
    this.setPosition(x, y);
    this.setSize(this.text.width, this.text.height);
    this.setInteractive({ useHandCursor: true });
    this.scene.add.existing(this);

    this.text.setTint(this.color);

    this.on("pointerover", () => this.text.setTint(this.hoverColor));
    this.on("pointerout", () => this.text.setTint(this.color));
    this.on("pointerup", () => scene.input.setDefaultCursor(null)); // the hand cursor doesn't reset unless you do this
  }

  public setVisible(value: boolean): this {
    this.text.setVisible(value);
    return super.setVisible(value);
  }

  public setOrigin(x: number, y: number): this {
    this.text.setOrigin(x, y);
    return super.setOrigin(x, y);
  }

  public setColor(color: integer) {
    this.color = color;
    this.text.setTint(this.color);
  }
}
