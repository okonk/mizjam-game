import { NPCData } from "./NPCData";
import { NPC } from "./NPC";
import { DiceData } from "./Dice/DiceData";
import { Constants } from "../Constants";

export class NPCFactory {
  public static get(id: integer): NPCData {
    switch (id) {
      case 0:
        return new NPCData("Priest", 24, 508, [], 1, "Its terrible. Without the kings magenta dice I dont know what we are going to do. The bandits will take over.");
      case 1:
        return new NPCData("Old Man", 312, 507, [], 1, "In this town the best dice player has the power. You have to help us get the kings dice back.");
      case 2:
        return new NPCData("King", 172, 506, [], 1, NPCFactory.kingInteractLogic);
      // room 1
      case 3:
        return new NPCData("Bandit", 218, 218, [1, 1, 1, 1, 1, 2, 2, 2, 2, 2], 2, "You will never defeat the boss.");
      case 4:
        return new NPCData("Bandit", 223, 223, [1, 1, 1, 1, 2, 2, 2, 3, 3], 4, "If you want to fight the boss you have to get through me.");
      case 5:
        return new NPCData("Bandit", 174, 174, [1, 1, 1, 2, 2, 2, 2, 3, 3, 3], 6, "Lets battle.");
      case 6:
        return new NPCData("Bandit", 221, 221, [1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 5], 9, "I will defeat you.");
      case 7:
        return new NPCData("Bandit", 318, 318, [2, 2, 2, 2, 2, 3, 3, 4, 4, 5, 5], 12, "I will take your dice.");
      // room 2
      case 8:
        return new NPCData("Bandit", 318, 318, [2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5], 17, "I will take your dice.");
      case 9:
        return new NPCData("Bandit", 174, 174, [3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6], 21, "You will never defeat the boss.");
      case 10:
        return new NPCData("Bandit", 221, 221, [4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7], 25, "I will take your dice.");
      case 11:
        return new NPCData("Bandit", 223, 223, [4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7], 29, "Lets battle.");
      case 12:
        return new NPCData("Bandit", 318, 318, [4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 8, 8], 33, "I will take your dice.");
      // room 3
      case 13:
        return new NPCData("Bandit", 318, 318, [5, 6, 6, 6, 6, 7, 7, 7, 8, 8, 8, 8, 9, 9, 10], 40, "I will take your dice.");
      case 14:
        return new NPCData("Bandit", 318, 318, [6, 6, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11], 48, "I will take your dice.");
      case 15:
        return new NPCData("Bandit", 318, 318, [7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11], 52, "I will take your dice.");
      case 16:
        return new NPCData("Bandit", 318, 318, [7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12], 56, "I will take your dice.");
      case 17:
        return new NPCData("Bandit", 318, 318, [8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 13], 60, "I will take your dice.");
      // boss room
      case 18:
        return new NPCData("Bandit", 318, 318, [9, 9, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14], 70, "The boss is waiting for you. But you have to get through me first.");
      case 19:
        return new NPCData("Bandit", 318, 318, [11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 15], 77, "I will take care of you so the boss doesnt have to.");
      case 20:
        return new NPCData("Bandit", 318, 318, [11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 15], 84, "The boss will rule this town.");
      // boss
      case 21:
        return new NPCData("Boss", 458, 458, [12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 15, 15, 15, 15, 16], 90, "I will defeat you and then take over the town!", true);
    }

    return undefined;
  }

  public static kingInteractLogic(scene: Phaser.Scene, npc: NPC) {
    if (npc.interactionCount == 1) {
      npc.showTextDialog("Welcome adventurer. I need your help. A bandit has stolen my magenta dice. Without it I have no power. The bandits will take over and destroy the town. Please get it back for me.");
    }
    else {
      const playerData = scene.registry.get("player");
      const playerDice: DiceData[] = playerData.dice.concat(playerData.inventory);

      if (!playerDice.find(d => d.color == 0xff0097)) {
        npc.showTextDialog("Please help me get my magenta dice back. I think the bandit is to the north.");
      }
      else if (!playerData.hasCompletedGame) {
        npc.showTextDialog("Thank you so much! The town is saved because of you. I dont know how to repay you.", () => { scene.scene.start(Constants.CREDITS) });
      }
      else {
        npc.showTextDialog("Thank you for getting the magenta dice back. You can use it for now.");
      }
    }
  }
}
