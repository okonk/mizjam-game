import { Map } from "./Map";
import { Entity } from "./Entity";
import { Constants } from "../Constants";
import { NPC } from "./NPC";
import { EntityType } from "./EntityType";
import { KeyMap } from "./KeyMap";

const Vector2 = Phaser.Math.Vector2;
type Vector2 = Phaser.Math.Vector2;

export class Player extends Entity {
  private keys: KeyMap;

  constructor(scene: Phaser.Scene, tileX: integer, tileY: integer, map: Map, keys: KeyMap) {
    super(scene, 25, tileX, tileY, map);

    this.keys = keys;
    this.entityType = EntityType.Player;

    this.scene.events.on("shutdown", () => this.onSceneShutdown(scene));
  }

  public checkInput() {
    if (this.isMoving()) return;

    if (this.keys.left.isDown || this.keys.leftAlt.isDown)
      this.move(Vector2.LEFT);
    else if (this.keys.right.isDown || this.keys.rightAlt.isDown)
      this.move(Vector2.RIGHT);
    else if (this.keys.up.isDown || this.keys.upAlt.isDown)
      this.move(Vector2.UP);
    else if (this.keys.down.isDown || this.keys.downAlt.isDown)
      this.move(Vector2.DOWN);
  }

  public update(time: number, delta: number) {
    this.checkInput();

    super.update(time, delta);
  }

  protected finishedMove() {
    const warp = this.map.getWarp(this.tileX, this.tileY);
    if (warp) {
      var playerData = this.scene.registry.get("player");
      playerData.mapId = warp.id;
      playerData.mapX = warp.x;
      playerData.mapY = warp.y;
      // Hack to fix onSceneShutdown overwriting mapX/mapY
      this.tileX = warp.x;
      this.tileY = warp.y;

      this.scene.scene.start(Constants.GAME);
    }
  }

  private onSceneShutdown(scene: Phaser.Scene) {
    var playerData = scene.registry.get("player");
    playerData.mapX = this.tileX;
    playerData.mapY = this.tileY;
  }

  public interact() {
    const x = this.tileX + this.facing.x;
    const y = this.tileY + this.facing.y;

    const entity = this.map.getEntity(x, y);
    if (entity) {
      (entity as NPC).interact();
    }
  }
}
