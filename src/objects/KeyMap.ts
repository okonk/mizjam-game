export class KeyMap {
  public inventory: Phaser.Input.Keyboard.Key;
  public down: Phaser.Input.Keyboard.Key;
  public up: Phaser.Input.Keyboard.Key;
  public left: Phaser.Input.Keyboard.Key;
  public right: Phaser.Input.Keyboard.Key;
  public interact: Phaser.Input.Keyboard.Key;
  public downAlt: Phaser.Input.Keyboard.Key;
  public upAlt: Phaser.Input.Keyboard.Key;
  public leftAlt: Phaser.Input.Keyboard.Key;
  public rightAlt: Phaser.Input.Keyboard.Key;
}
