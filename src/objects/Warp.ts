

export class Warp {
  public id: integer;
  public x: integer;
  public y: integer;

  constructor(id: integer, x: integer, y: integer) {
    this.id = id;
    this.x = x;
    this.y = y;
  }
}
