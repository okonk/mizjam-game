import { Map } from "./Map";
import { Entity } from "./Entity";
import { Constants } from "../Constants";
import { EntityType } from "./EntityType";
import { DiceData } from "../objects/Dice/DiceData";
import { DiceFactory } from "../objects/Dice/DiceFactory";
import { NPCData } from "./NPCData";

const Vector2 = Phaser.Math.Vector2;
type Vector2 = Phaser.Math.Vector2;

export class NPC extends Entity {
  private id: integer;
  private spawnX: integer;
  private spawnY: integer;

  private lastMove: integer;
  private moveSpeed: integer;

  public npcData: NPCData;
  public interactionCount: integer;

  public icon: string;
  public dice: DiceData[];
  public goalScore: integer;

  private static directions: Vector2[] = [Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT];

  constructor(scene: Phaser.Scene, id: integer, npcData: NPCData, tileX: integer, tileY: integer, map: Map) {

    let dice;
    let spawnX = tileX;
    let spawnY = tileY;
    let interactionCount = 0;
    let oldState = map.getState()[id];
    if (oldState) {
      dice = oldState.dice;
      tileX = oldState.x;
      tileY = oldState.y;
      interactionCount = oldState.interactionCount;
    }

    super(scene, npcData.graphicId, tileX, tileY, map);

    this.npcData = npcData;
    this.id = id;
    this.entityType = EntityType.NPC;
    this.name = npcData.name;
    this.spawnX = spawnX;
    this.spawnY = spawnY;
    this.speed = 600;
    this.moveSpeed = 2000;
    this.lastMove = Date.now() - Phaser.Math.Between(0, 1000);
    this.icon = `colored_transparent-${this.npcData.iconId}.png`;
    this.dice = dice ?? this.npcData.dice.map(d => DiceFactory.get(d));
    this.goalScore = this.npcData.goalScore;
    this.interactionCount = interactionCount;

    this.setInteractive();
    this.on("pointerup", () => this.interact());
  }

  public update(time: number, delta: number) {
    if (!this.npcData.fixed)
      this.doRandomMove();

    super.update(time, delta);
  }

  private doRandomMove() {
    if (Date.now() - this.lastMove >= this.moveSpeed) {
      let direction: Vector2;

      const diffX = this.tileX - this.spawnX;
      const diffY = this.tileY - this.spawnY;
      if (Math.abs(diffX) > 6 || Math.abs(diffY) > 6) {
        if (Math.abs(diffX) > Math.abs(diffY)) {
          direction = diffX < 0 ? Vector2.RIGHT : Vector2.LEFT;
        }
        else {
          direction = diffY < 0 ? Vector2.DOWN : Vector2.UP;
        }
      }
      else {
        direction = NPC.directions[Phaser.Math.Between(0, 3)];
      }

      this.move(direction);

      this.lastMove = Date.now();
    }
  }

  private getInteractText(): string {
    if (!this.playerHasEnoughDice()) {
      return "You dont have enough dice equipped to battle.";
    }
    else if (this.npcData.dice.length > 3 && this.dice.length <= 3) {
      return "You beat me too many times. I dont have enough dice left!";
    }

    return this.npcData.interact as string;
  }

  public showTextDialog(text: string, next: () => void = null) {
    if (!next) {
      next = () => this.scene.scene.resume(Constants.GAME);
    }

    this.scene.scene.pause(Constants.GAME);
    this.scene.scene.launch(Constants.DIALOG, { next, npc: this, text });
  }

  public interact() {
    this.interactionCount++;

    if (typeof this.npcData.interact === "string") {
      this.showTextDialog(this.getInteractText(), () => this.onNext());
    }
    else {
      this.npcData.interact(this.scene, this);
    }
  }

  private onNext() {
    if (this.dice.length >= 3 && this.playerHasEnoughDice()) {
      this.scene.scene.start(Constants.DICE, { npc: this });
    }
    else {
      this.scene.scene.resume(Constants.GAME);
    }
  }

  private playerHasEnoughDice(): boolean {
    return this.scene.registry.get("player").dice.length >= 3;
  }

  public saveState(storage: any) {
    let state = {
      x: this.tileX,
      y: this.tileY,
      dice: this.dice,
      interactionCount: this.interactionCount
    };

    storage[this.id] = state;
  }
}
