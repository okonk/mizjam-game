import { DiceData } from "./Dice/DiceData";
import { NPC } from "./NPC";

export class NPCData {
  public name: string;
  public graphicId: integer;
  public iconId: integer;
  public dice: integer[];
  public goalScore: integer;
  public interact: string | ((scene: Phaser.Scene, npc: NPC) => void);
  public fixed: boolean;

  constructor(
    name: string, graphicId: integer, iconId: integer, dice: integer[], goalScore: integer,
    interact: string | ((scene: Phaser.Scene, npc: NPC) => void), fixed: boolean = false
  ) {
    this.name = name;
    this.graphicId = graphicId;
    this.iconId = iconId;
    this.dice = dice;
    this.goalScore = goalScore;
    this.interact = interact;
    this.fixed = fixed;
  }
}
