import { DiceData } from "./objects/Dice/DiceData";

export class Utils {
  public static removeDice(inventory: DiceData[], index: integer): DiceData {
    const removed = inventory[index];
    inventory.splice(index, 1);
    return removed;
  }
}
