Before starting:
- Looked at spritesheet, came up with game idea. Dungeon crawler with dice gambling component.

30 mins:
- Got environment set up with Phaser, Typescript, Webpack

1h 30m:
- Created basic map to test with
- Loaded map in game

3h 30m:
- Set up TexturePacker
- Got player sprite displaying
- Camera follows player
- Player input very buggy
- Nice resource https://medium.com/swlh/grid-based-movement-in-a-top-down-2d-rpg-with-phaser-3-e3a3486eb2fd

4h:
- Fixed input bug (bad checking of "goal" position)

4h 15m:
- Blocked tiles checking before move

6h:
- Can switch maps (kinda buggy though)

6h 20m:
- Implemented background colors

6h 55m:
- NPCs that move around randomly

8h:
- Interact with NPCs
- Can't walk through NPCs now/they can't walk through you
- Added basic Scene for the dice game

8h 35m:
- Dice roll simulation/display

10h 20m:
- Got fonts working...

12h:
- Most of DiceScene UI elements in place

13h:
- First player round of Dice game implemented

13h 50m:
- Other rounds implemented
- AI implemented

15h 25m:
- Basic menu Scene
- Buttons change colour and cursor on hover
- Refactored DiceFactory/Dice

17h:
- Basic end scene for dice game
- Dice game uses dice from player
- Dice game uses npc name/dice/icon

17h 45m:
- DiceEnd scene done (just needs timeout to next scene)
- Gameplay appears to be working without bugs

20h:
- Started work on inventory scene
- Working out how to use scrollbar library

21h 45m:
- Inventory scene finished

22h 20m:
- Fix bug switching back to dice scene
- Fix bug switching maps
- Fix bug in dice scene rolling

22h 40m:
- Save NPC state to registry

24h 30m:
- NPC factory
- Dialog scene working nicely


25h 15m:
- NPCs have interact text
- When interactnig with npc they don't battle if you or they have less than 3 dice
- Bug fixes and cleanup

26h 15m:
- Tried transitions but couldn't get them working properly so removed
- Fix to word wrapping
- More tidy up
- Small improvement to npc ai to end if he will win

28h:
- Added 16 dice
- Added ability to call function for npc interact
- Added logic for king npc
- Added UI button for inventory

29h:
- Credits scene done
- Tutorial scene done

31h:
- Added more maps/npcs
- More polish

32h 25m:
- Added all npcs
- Added all maps
